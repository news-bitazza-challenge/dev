# Requirements
- git
- docker 18.09
- docker-compose 1.23
- node.js 10

# Development

### 1. Clone
```
$ mkdir news-bitazza-challenge && cd news-bitazza-challenge
$ git clone https://gitlab.com/news-bitazza-challenge/dev.git && cd dev
$ ./clone_all
```

### 2. Install all dependencies
```
$ ./install_dependencies
```

### 3. Run migrates
```
$ ./migrates
```

### 4. Run server
```
$ docker-compose up -d                                  // run background 
$ docker-compose up <back-end, web, web-console>        // run separate
```

### 5. Server
```
http://localhost:8000 for web
http://localhost:8001 for web console
```

### 6. Stop server
```
$ docker-compose stop
```

# Deployment

### 1. Clone
```
$ mkdir news-bitazza-challenge && cd news-bitazza-challenge
$ git clone https://gitlab.com/news-bitazza-challenge/dev.git && cd dev
$ ./clone_all
```

### 2. Deployment working space
```
$ cd deployment
```

### 3. build
```
~/deployment$ ./build
```

### 4. run server
```
~/deployment$ ./start
```

## 5. Server
```
http://150.95.31.201:8000  //for web
http://150.95.31.201:8001  //for web console
```

# Document
### Data mock 
- generate random news (50 news) 
- admin user
```
username: admin
password: 123456
```

### Postman
APIs documents
```
docs/news-bitazza-challenge.postman_collection.json
```